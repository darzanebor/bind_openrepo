class bind::zones {
    file { $bind::params::bind_zones_dir:
        ensure => directory,
        owner => 'bind',
    }
    $bind::params::domains.each | $domain | {
      file { $domain['path']:
              ensure => present,
              content => epp('bind/zones.file.epp',  {'domain' =>  $domain}),
              owner => 'bind',
              notify => Service['bind'],
      }
    }
}

#certbot-auto certonly -w /certs --standalone -d $DOMAIN --preferred-challenges http --agree-tos -n -m $EMAIL --keep-until-expiring
