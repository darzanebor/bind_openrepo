class bind {

  include bind::startpack
  include bind::params

  package { 'dns_server':
      name => "bind9",
      ensure => present,
  }

  file { $bind::params::bind_logs:
      ensure => directory,
      recurse => true,
      owner => 'bind',
      group => ['bind'],
  }

  file { $bind::params::bind_keys_dir:
      ensure => directory,
      source => 'puppet:///modules/bind/keys', #temp
      recurse => 'remote',
  }

  include bind::zones

  file { $bind::params::file_coptions:
      content => epp('bind/named.conf.options.epp', $bind::params::named_conf_options),
      notify => Service['bind'],
  }

  file { $bind::params::file_clocal:
    content => epp('bind/named.conf.local.epp', $bind::params::named_conf_local),
    notify => Service['bind'],
  }

  file { $bind::params::file_named_conf:
    content => epp('bind/named.conf.epp', $bind::params::named_conf),
    notify => Service['bind'],
  }

  file { '/etc/systemd/system/bind.service':
    source => 'puppet:///modules/bind/bind.service',
    notify => Service['bind'],
  }

  service { 'bind':
    ensure => running,
    enable => true,
  }
}
