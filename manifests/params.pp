class bind::params{
  $notification = "#!!!                                                                      !!!\n#!!!Attention this file is autocrated and would be modified on next change!!!\n#!!!                                                                      !!!"
  $domains = [
        {
         'name' => '"domain.ru"',
         'path' => '/etc/bind/zones/db.domain.ru',
         'ttl' => '604800',
         'contacts' => 'nsa.domain.ru. postmaster.domain.ru.',
         'serial' => '198406196;',
         'refresh' => '10800;',
         'retry' => '1800;',
         'expire' => '1209600;',
         'minimum' => '21600;',
         'nsrecords' => [
            '0.ns.domain.ru.',
            '1.ns.domain.ru.',
            '2.ns.domain.ru.',
            '3.ns.domain.ru.',
            '4.ns.domain.ru.'
          ],
          'mxrecords' => [
            {
              priority => '10',
              hostname => 'mx.domain.ru.',
            },
          ],
          'arecords' => [
            {
              ip_address => '8.8.8.8',
              hostname => 'domain.ru.',
            },
          ],
          'cnames' => [
            {
              src => 'www',
              dst => 'go.domain.ru.',
            },
            {
              src => 'go',
              dst => 'ip.domain.com.',
            },
          ],
          'caarecords' => [
            {
              record => 'domain.ru.',
              value => '0 issue "letsencrypt.org"',
            },
            {
              record => 'domain.ru.',
              value => '0 iodef "mailto:reports-mail@domain.ru"',
            },
          ],
          'txtrecords' => [
            {
              record => 'domain.ru.',
              value => '"v=spf1 redirect=_spf.domain.ru"',
            },
            {
              record => 'mail._domainkey',
              value => '"v=DKIM1; k=rsa; t=s; p="',
            },
            {
              record => '_dmarc.domain.ru.',
              value => '"v=DMARC1; p=reject; sp=reject; rua=mailto:reports@domain.ru"',
            },
          ],
          'includes' => [
            '/etc/bind/keys/Kdomain.ru.+007+44261.key',
            '/etc/bind/keys/Kdomain.ru.+007+54675.key',
          ],
        },
  ]
  $named_conf_options = {
        'cache_dir' => '"/var/cache/bind"',
        'listen_on' => "any;",
        'allow_transfer' => "8.8.8.8; 8.8.4.4; 1.1.1.1;",
        'allow_notify' => "7.7.7.7;",
        'allow_recursion' => "none;",
        'allow_update' => "none;",
        'empty_zones_enable' => "no;",
        'tcp_clients' => "500;",
        'dnssec_enable' => "yes;",
        'key_directory' => '"/etc/bind/keys"',
        'enable_forwarders' => false,
        'forwarders' => "8.8.8.8; 4.4.4.4;",
        'auth_nx_domain' => "yes;",
        'listen_on_v6' => "none;",
        'custom_logging_dir' => "/var/log/named/",
        'custom_logging_severity' => "info;",
        'notification' =>  $notification,
  }
  $named_conf_local = {
        'notification' =>  $notification,
        'domains' => $domains,
  }
  $named_conf = {
        'notification' =>  $notification,
  }
      $bind_logs = "/var/log/named"
      $bind_keys_dir = "/etc/bind/keys"
      $bind_zones_dir = "/etc/bind/zones"
      $file_coptions = "/etc/bind/named.conf.options"
      $file_clocal = "/etc/bind/named.conf.local"
      $file_named_conf = "/etc/bind/named.conf"
}
