class bind::startpack {
  $startpack = ['nano','dnsutils','net-tools','haveged']
  package { $startpack:
    ensure => present,
  }
}
